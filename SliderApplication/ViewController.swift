//
//  ViewController.swift
//  SliderApplication
//
//  Created by Jer Cherng Law on 3/14/16.
//  Copyright © 2016 CS 481. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var UserInput: UITextField!
    @IBOutlet weak var sliderOutput: UILabel!
    @IBOutlet weak var convertedOutput: UILabel!

    @IBOutlet weak var largeUnits: UILabel!
    @IBOutlet weak var smallUnits: UILabel!
    @IBOutlet weak var convertedUnits: UILabel!
    @IBOutlet weak var sliderOutputUnits: UILabel!
    @IBOutlet weak var sliderMaxValue: UILabel!
    
    @IBAction func convertInput(sender: UIButton) {
        let input:Float? = Float(UserInput.text!)
        convertedOutput.text = String(input! * 1000.0)
    }

    var colors = ["lbs to kg","ft to cm","m/s to ft/s","hr to s"]
    
    var pickerDataLargeUnits = ["lbs", "ft", "m/s", "hr"]
    var pickerDataSmallUnits = ["Oz", "in", "", ""]
    var pickerDataOutputUnits = ["kg", "m", "ft/s", "s"]
    
    var sliderUnits = ["12", "16"]
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return colors.count
    }

    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return colors[row]
    }
    
    @IBOutlet weak var sliderOutlet: UISlider!
    
    @IBAction func modifySmallUnits(sender: UISlider) {
        
        let currentValue:Int? = sender.value
        
        sliderOutput.text = "\(currentValue)"
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        largeUnits.text = pickerDataLargeUnits[row]
        smallUnits.text = pickerDataSmallUnits[row]
        convertedUnits.text = pickerDataOutputUnits[row]
        sliderOutputUnits.text = pickerDataOutputUnits[row]
        
        if( row == 1 || row == 0) {
            sliderMaxValue.text = sliderUnits[row]
        }
        
        if( row != 1 || row != 0) {
            sliderOutlet.enabled = false
        }
        
    }
    
    func convertMassUnits(userInputLbs: Int, userInputOz: Int) -> Float {
        
    }

    func convertLengthUnits() -> Float {
        
    }

    func convertSpeedUnits() -> Float {
        
    }

    func convertTimeUnits() -> Int {
        
    }

    
}

